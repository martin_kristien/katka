# Webpage with online booking

This web application allows users to view a webpage for Denthyka-Dental Hygiene Ambulance in Partizanske, Slovakia. The application based on AngularJS allows users to make online booking.
Admin access to the application provides interface for viewing, altering, and deleting bookings.

The web server is started withing python SimpleServer.py file

The application is currently under development. The language of the application view is Slovak.

# Running
to start the server, run:
`python SimpleServee.py`
relevant python version is 2.7.11

you can view the webpage by typing in a web browser `http://localhost:8000/`
The relevant port number may differ, check the output of python server for actual port number.

# Author
The application is currently being developed by Martin Kristien.