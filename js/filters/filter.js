app.filter('time', function() {
	return function(i){
		if(i<10){
			return "0"+i+":00";
		}else{
			return ""+i+":00";
		}
	};
});