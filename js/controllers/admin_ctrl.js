var available=true;
app.controller("admin_ctrl", function($scope, $rootScope, $http,$filter){
	$scope.today = new Date();
	$scope.offset = 0;		//curretn offset of weeks from today to the future
	$scope.maxoffset = 4; 	//number of week you can move to the future
	// $scope.daysWeek = ["Pondelok", "Utorok", "Streda", "\u0160tvrtok", "Piatok","Sobota", "Nedela"];
	$scope.daysWeek = ["Po","Ut","St","\u0160t","Pi","So","Ne"];
	$scope.days = new Array(7);

	//initialize days data
	for(i=0;i<$scope.daysWeek.length;i++){
		$scope.days[i] = new Object();
		$scope.days[i].day = $scope.daysWeek[($scope.today.getDay() + i -1) % 7];	//name of week day
		$scope.days[i].date = new Date();	//corresponding date
		$scope.days[i].date.setDate($scope.today.getDate()+i);

		//data, bookings
		$scope.days[i].hours = new Array(7);	//array of working hours
		var hour;
		//initialize hours, default is not booked
		for(j=0;j<$scope.days[i].hours.length;j++){
			hour = new Object();
			hour.t = 8+j;
			if($scope.days[i].day == "So" || $scope.days[i].day == "Ne"){
				hour.state = 2;	//not available
			}else{
				hour.state = 0;	//free
			}
			$scope.days[i].hours[j] = hour
		}
		//add information from the server about bookings
		loadDay($http, $scope.days[i].date, $scope.days[i].hours);
	}
	//current order
	$scope.order = new Object();
	$scope.formOn = false;
	$scope.order.user = new Object();
	$scope.order.user.name = "";
	$scope.order.user.surname = "";
	$scope.order.user.email = "";
	$scope.order.user.phone = "";
	$scope.order.user.note = "";
	$scope.order.vacate = false;
	$scope.order.book = false;
	$scope.order.remove = false;

	//create booking form
	$scope.book = function(hour,day){
		if($scope.formOn){
			//if booking form is already on, do nothing
			return;
		}
		$scope.invalid=false;	//indicates validity of form data
		$scope.formOn=true;
		
		$scope.order.hour = hour;
		$scope.order.day = day;


	}
	//turn off and reset booking form
	$scope.cancelOrder = function(){
		$scope.formOn=false;
		if($scope.order.user==null)
			return;
		$scope.order.user.name="";
		$scope.order.user.surname="";
		$scope.order.user.email="";
		$scope.order.user.phone="";
		$scope.order.user.note="";
		$scope.order.vacate = false;
		$scope.order.book = false;
		$scope.order.remove = false;
	};
	//finalize booking
	$scope.sendOrder = function(){
		var user = $scope.order.user;
		if(user.name==""||user.surname==""||
			typeof user.email=="undefined" || user.email==""||
			user.phone==""){
			$scope.invalid=true;
			return;
		}

		//send order
		var date = $scope.order.day.date;
		var name = date.getFullYear()+"_"+(1+date.getMonth())+"_"+
			date.getDate();

		$http.post("/data/objednavky/"+name+"_"+$scope.order.hour.t+".json",angular.toJson($scope.order.user,true)).
		success(function(){
			$scope.order.hour.state = 1;
			$http.post("/data/hodiny/"+name+".json",angular.toJson($scope.order.day.hours,true)).success(function(){
				console.log("hours updated");
				alert("termin objednany");
				$scope.cancelOrder();

			});
		});
	};
	//jump one week to the future
	$scope.loadNext = function(){
		$scope.offset++;

		var dateStart = new Date();
		dateStart.setDate(dateStart.getDate()+7*$scope.offset);
		for(i=0;i<$scope.daysWeek.length;i++){
			$scope.days[i].date = angular.copy(dateStart);
			dateStart.setDate(dateStart.getDate()+1);
			loadDay($http, $scope.days[i].date, $scope.days[i].hours);
		}
	};
	//jump one week to the past
	$scope.loadPrevious = function(){
		$scope.offset--;
		var dateStart = new Date();
		dateStart.setDate(dateStart.getDate()+7*$scope.offset);
		for(i=0;i<$scope.daysWeek.length;i++){
			$scope.days[i].date = angular.copy(dateStart);
			dateStart.setDate(dateStart.getDate()+1);
			loadDay($http, $scope.days[i].date, $scope.days[i].hours);
		}
	}

function loadDay(http,date,hours){
	var name = date.getFullYear()+"_"+(date.getMonth()+1)+
				"_"+date.getDate()+".json";
	http.get("/data/hodiny/"+name).then(
	function(newdata){

		for(i=0;i<7;i++){
			hours[i]=newdata.data[i];
			if(hours[i].state == 0)
				available = true;
		}
		console.log('found');
		if((date.getDay()+1)%7 == new Date().getDate()
			&& !available){
			console.log("last date loaded and no available found");
			$scope.loadNext();
		}
	},
	function(error){
		var hour;
		for(i=0;i<7;i++){
			hour = hours[i];
			hour.t = 8+i;
			if(date.getDay() == 6 || date.getDay() == 0){
				hour.state = 2;	//not available
			}else{
				hour.state = 0;	//free
			}
		}
		console.log('not found');
		http.post("/data/hodiny/"+name,angular.toJson(hours,true)).success(function(){
			console.log("posted");
			available = true;
		});
	})
}
	$scope.viewHour = function(hour,day){
		if(hour.state == 1){
			console.log("hour booked, loading booking info");
			//the hour is booked, load booking info
			var date = day.date;
			var name = date.getFullYear()+"_"+(1+date.getMonth())+"_"+date.getDate();

			$http.get("/data/objednavky/"+name+"_"+hour.t+".json").then(function(newdata){
				$scope.order.user = newdata.data;
				console.log(newdata.data);
			}, function(error){
				alert("objednavka sa nenasla");
			});
			$scope.book(hour,day);
		}else if(hour.state == 0 || hour.state == 2){
			console.log("free hour, can make booking");
			//no booking, load booking form
			$scope.book(hour,day);
		}		
	}
	$scope.saveHour = function(){
		console.log("saving changes");
		if($scope.order.book){
			console.log("sending order");
			$scope.sendOrder();

		}
		else if($scope.order.vacate){
			$scope.order.hour.state = 0;
			var date = $scope.order.day.date;
			var name = date.getFullYear()+"_"+(date.getMonth()+1)+
				"_"+date.getDate()+".json";
			$http.post("/data/hodiny/"+name,angular.toJson($scope.order.day.hours,true)).success(function(){
				alert("termin uvolneny");
			});
			$scope.cancelOrder();
		}
		else if($scope.order.remove){
			$scope.order.hour.state = 2;
			var date = $scope.order.day.date;
			var name = date.getFullYear()+"_"+(date.getMonth()+1)+
				"_"+date.getDate()+".json";
			$http.post("/data/hodiny/"+name,angular.toJson($scope.order.day.hours,true)).success(function(){
				alert("termin odstraneny");
			});
			$scope.cancelOrder();
		}

		// $scope.cancelOrder();
	}
});