import SimpleHTTPServer
import SocketServer
import logging
import urlparse
import os, errno
import subprocess
PORT = 8000

class ServerHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_GET(self):
        # logging.error(self.headers)
        print self.path[1:4]

        if self.path[1:4] == 'php':
            print 'je to php'
            proc = subprocess.Popen("php "+self.path[1:], shell=True, stdout=subprocess.PIPE)
            script_response = proc.stdout.read()
            print 'result:' + script_response +'koniec'
            self.send_response(200)
            self.send_header("Content-length", len(script_response))
            self.end_headers()
            self.wfile.write(script_response)
        elif self.path[1:6] == 'admin':
            if not self.authenticate():
                print 'authentication requered'
                return
            SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)
        elif self.path == "/" or self.path =="/index.html":
            print "empty request"
            self.path = "/index.html"
            SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)
        elif self.path[1:16] == 'data/objednavky' or self.path[1:16]=="SimpleServer.py":
            if not self.authenticate():
                return
            SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)
        else:
            # print 'nie je to php'
            SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)
        


    def do_POST(s):
        # logging.error(s.headers)
        length = int(s.headers['Content-Length'])
        data = s.rfile.read(length).decode('utf-8')
        # print data
        # print s.path
        filename = s.path[1:]
        #check directory exists
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc: #guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        #create/open file
        text = open(filename,"w")
        text.write(data)
        text.close

        s.send_response(201)
        s.send_header("Content-type", "text/html")
        s.end_headers()
    def do_PUT(s):
        s.do_POST()
    def do_DELETE(s):
        try:
            os.remove(s.path[1:])
        except OSError as e: # this would be "except OSError, e:" before Python 2.6
            if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
                raise # re-raise exception if a different error occured
        s.send_response(200)
        s.end_headers()
    def authenticate(self):
        print 'headers'
        print self.headers
        code = self.headers.get('Authorization',"");
        print "code is " + code
        if code == "":
            self.send_response(401,"Authorisation missing")
            self.send_header("WWW-Authenticate","Basic")
            self.end_headers()
            return False
        else:
            if code != "Basic a2F0a2E6emlha292YQ==":
                self.send_response(401, "Authorisation invalid")
                self.send_header("WWW-Authenticate","Basic")
                self.end_headers()
                return False
        return True




Handler = ServerHandler

httpd = SocketServer.TCPServer(("", PORT), Handler)

print "serving at port", PORT
httpd.serve_forever()